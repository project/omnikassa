
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * OmniKassa
 * OmniKassa Commerce

INTRODUCTION
------------

This modules gives a settings page for the Rabobank Omnikassa paymentmethod.

A submodule for the commerce module has been added.
We will work on the implementation of Übercart as well.

This project is sponsored by Exed Internet.


INSTALLATION
------------

Active the OmniKassa module and fill the settings at 
admin/config/services/omnikassa. This gives you access to OmniKassa.
If you use the Commerce module(s) for your ecommerce solution, you can 
activate the OmniKassa Commerce module. This gives you all the payment 
options for Commerce. 


OMNIKASSA
---------

The Omnikassa module provides settings capabilities for the submodule(s).
The following Omnikassa payment options are available:
 * iDEAl
 * Minitix
 * Visa
 * Mastercard
 * Maestro
 * Acceptgiro
 * Rembours (payment on delivery)


OMNIKASSA COMMERCE

The OmniKassa Commerce module implements the OmniKassa settings as payment
options as for the Commerce Module.
